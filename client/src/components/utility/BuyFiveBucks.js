import React, {Component} from 'react';
import BtnBase from "../btn/BtnBase";
import 'bootstrap/dist/css/bootstrap.min.css'

class BuyFiveBucks extends Component {

  constructor(props) {
    super(props)
    this.handlerOnClick = this.handlerOnClick.bind(this)
  }


  handlerOnClick = async() => {
    try {
      await fetch('/api/refill/?id=' + this.props.idUser)

    } catch (err) {
      console.log("error", err)
    }
  }

  render() {
    console.log(this.props.idUser)
    return (
      <div className="float-left">
        <BtnBase classArgs={"btn btn-success float-right"}  text="Acheter 5 Bucks" handlerOnClick={this.handlerOnClick}/>
      </div>
    )
  }
}

export default BuyFiveBucks
