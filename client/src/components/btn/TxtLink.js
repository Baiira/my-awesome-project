import React, {Component} from 'react';

class TxtLink extends Component {
  handleClick = () => {
    this.props.handlerOnClick(this.props.dataToPrint)
  }
  render() {
    return (
      <button onClick={this.handleClick} className="float-left mt-4 mr-1 w-auto h-auto btn" style={this.props.style} data-toggle="modal" data-target="#exampleModal">
        {this.props.text}
      </button>
    )
  }
}

export default TxtLink
