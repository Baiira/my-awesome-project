import React, {Component} from 'react';

class BtnBase extends Component {

  render() {
    return (
      <button className={this.props.classArgs} onClick={this.props.handlerOnClick.bind(this)}>
        {this.props.text}
      </button>
    )
  }
}

export default BtnBase
