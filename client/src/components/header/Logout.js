import React, {Component} from 'react';
import BtnBase from "../btn/BtnBase";
import BuyFiveBucks from "../utility/BuyFiveBucks";

class Logout extends Component {
  onClickhandler1 = () => {
    this.props.onClickHandler()
  }
  
  constructor(props) {
    super(props)
    this.onClickHandler1 = this.onClickhandler1.bind(this)
  }
  
  render() {
    return (
      <div className={this.props.styleZone}>
        <BuyFiveBucks idUser={this.props.idUser}/>
        <BtnBase text="Se deconnecter" classArgs={"btn btn-danger float-right"} handlerOnClick={this.onClickHandler1}/>
      </div>
    )
  }
}

export default Logout
