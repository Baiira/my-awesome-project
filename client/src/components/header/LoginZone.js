import React, {Component} from 'react';
import TxtLink from "../btn/TxtLink";

class LoginZone extends Component {
  onClickHandler1 = () => {
    this.props.onClickHandler((this.state.nameInput !== "" && this.state.passwordInput !== ""))
  }

  constructor(props) {
    super(props)
    this.state = {
      nameInput: "",
      passwordInput: "",
    }
    this.onChangeHandler = this.onChangeHandler.bind(this)
    this.onChangeHandler1 = this.onChangeHandler1.bind(this)
    this.onClickHandler1 = this.onClickHandler1.bind(this)
  }

  onChangeHandler(e) {
    this.setState({nameInput: e.target.value})
  }

  onChangeHandler1(e) {
    this.setState({passwordInput: e.target.value})
  }

  render() {
    return (
      <div className={this.props.styleZone}>
        <TxtLink text="Se connecter" handlerOnClick={this.props.onClickModal.bind(this)}
                 style={{backgroundColor: "green"}}/>
        <TxtLink text="S'inscrire" handlerOnClick={this.props.onClickModal.bind(this)} dataToPrint={2}/>
        <TxtLink text="Mot de passe?" handlerOnClick={this.props.onClickModal.bind(this)} dataToPrint={1}/>
      </div>
    )
  }
}

export default LoginZone
