import React, {Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import BtnBase from "../btn/BtnBase";

class CardBase extends Component {

  handlerOnClick = async () => {
    try {
      const res = await fetch('/api/dino/get/?id=' + this.props.dino.key + '&idUser=' + this.props.idUser)

      const responseJson = await res.json()

      this.props.updateMsg(responseJson.message)

    } catch (err) {
      console.log("error card: " + err)
    }
  }

  constructor(props) {
    super(props)
    this.handlerOnClick = this.handlerOnClick.bind(this)
  }

  render() {
    return (
      <div className={"card h-25 mt-4 mx-5 float-left"} style={{width: 200}}>
        Race: {this.props.dino.type}
        <br/>
        Nom: {this.props.dino.name}
        <br/>
        Description: {this.props.dino.description}
        <br/>
        Prix: {this.props.dino.price} Bucks
        {this.props.isLogged ? (
          <BtnBase classArgs="btn btn-primary" text="Je le veux !" handlerOnClick={this.handlerOnClick}/>
        ) : null}
      </div>
    )
  }
}

export default CardBase