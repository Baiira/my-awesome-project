import React, {Component} from 'react';

class Input extends Component {
  
  render() {
    return (
      <input type={this.props.type} name={this.props.name} placeholder={this.props.placeholder}
             onChange={this.props.onChangeHandler} className="form-control" required={this.props.required ? true : false}/>
    )
  }
}

export default Input
