import React, {Component} from 'react'
import ModalConnect from "./Inner/ModalConnect";
import ModalSubscribe from "./Inner/ModalSubscribe";
import ModalLostPassword from "./Inner/ModalLostPassword";

class Modals extends Component {
  handleClick = () => {
    this.props.handlerOnClick(0)
  }

  render() {
    if (!this.props.shouldRender) {
      return null
    }
    let innerModal = null
    console.log(this.props.modalToRender)
    switch (this.props.modalToRender) {
      case 1:
        innerModal = (<ModalLostPassword/>)
        break
      case 2:
        innerModal = (<ModalSubscribe/>)
        break
      default:
        innerModal = (<ModalConnect handlerOnClick={this.props.connectUser.bind(this)}/>)
        break
    }
    return (
      <div>
        {innerModal}
        <div className="w-100 h-100 position-fixed"
             style={{backgroundColor: '#3d3d3d', top: 0, left: 0, right: 0, bottom: 0, opacity: 0.5, zIndex:10}}
             onClick={this.handleClick}>
        </div>
      </div>
    )
  }
}

export default Modals
