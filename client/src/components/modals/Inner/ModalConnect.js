import React, {Component} from 'react'
import Input from "../../input/Input";
import BtnBase from "../../btn/BtnBase";

class ModalConnect extends Component {

  onClickHandler1() {
    this.props.handlerOnClick(this.state.nameInput, this.state.passwordInput)
  }

  constructor(props) {
    super(props)
    this.state = {
      nameInput: '',
      passwordInput: ''
    }
    //this.sendData = this.sendData.bind(this)
    this.onChangePassword = this.onChangePassword.bind(this)
    this.onChangeUser = this.onChangeUser.bind(this)
    this.onClickHandler1 = this.onClickHandler1.bind(this)
  }

  onChangePassword(e) {
    this.setState({
      passwordInput: e.target.value
    })
  }

  onChangeUser(e) {
    this.setState({
      nameInput: e.target.value
    })
  }
/*
  async sendData() {
    console.log(this.state)
    try {
      await fetch('http://localhost:5000/post/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user: this.state.user,
          password: this.state.password
        })
      }).then((response) => response.json())
        .then((responseData) => {
          console.log(responseData);
        })
    } catch (error) {
      console.error(error);
    }
  }
*/
  render() {
    return (
      <div className="w-25 rounded p-2 position-fixed"
           style={{opacity: 1, transform: "translate(150%, -50%)", backgroundColor: '#fff', zIndex: 15}}>
        <form>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email address</label>
            <Input type="text" className="form-control" placeholder="Enter email"
                   onChangeHandler={this.onChangeUser} required={true}/>
            <small id="emailHelp" className="form-text text-muted">
              We'll never share your email with anyone else.
            </small>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Password</label>
            <Input type="password" className="form-control" placeholder="Password"
                   onChangeHandler={this.onChangePassword} required={true}/>
          </div>
          <div className="form-check">
            <input type="checkbox" className="form-check-input" id="exampleCheck1"/>
            <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
          </div>
          <BtnBase handlerOnClick={this.onClickHandler1} classArgs="btn btn-primary" text="Se connecter"/>
        </form>
      </div>
    )
  }
}

export default ModalConnect
