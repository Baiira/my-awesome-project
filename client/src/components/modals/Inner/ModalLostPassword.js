import React, {Component} from 'react'
import Input from "../../input/Input";
import BtnBase from "../../btn/BtnBase";

class ModalLostPassword extends Component {

  constructor(props) {
    super(props)
    this.state = {
      user: '',
      password: ''
    }
    //this.sendData = this.sendData.bind(this)
    this.onChangePassword = this.onChangePassword.bind(this)
    this.onChangeUser = this.onChangeUser.bind(this)
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    })
  }

  onChangeUser(e) {
    this.setState({
      user: e.target.value
    })
  }
/*
  async sendData() {
    console.log(this.state)
    try {
      await fetch('http://localhost:5000/post/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user: this.state.user,
          password: this.state.password
        })
      }).then((response) => response.json())
        .then((responseData) => {
          console.log(responseData);
        })
    } catch (error) {
      console.error(error);
    }
  }
*/
  render() {
    return (
      <div className="w-25 rounded p-2 position-fixed"
           style={{opacity: 1, transform: "translate(150%, -50%)", backgroundColor: '#fff', zIndex: 15}}>
        <form>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email</label>
            <Input type="text" className="form-control" placeholder="Enter email" onChangeHandler={this.onChangeUser}/>
          </div>
          <BtnBase classArgs="btn btn-primary" text="Envoyer"/>
        </form>
      </div>
    )
  }
}

export default ModalLostPassword
