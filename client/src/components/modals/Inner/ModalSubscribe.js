import React, {Component} from 'react'
import Input from "../../input/Input";
import BtnBase from "../../btn/BtnBase";

class ModalSubscribe extends Component {

  constructor(props) {
    super(props)
    this.state = {
      user: '',
      password: ''
    }
    this.onChangePassword = this.onChangePassword.bind(this)
    this.onChangeUser = this.onChangeUser.bind(this)
  }

  onChangePassword(e) {
    this.setState({
      password: e.target.value
    })
  }

  onChangeUser(e) {
    this.setState({
      user: e.target.value
    })
  }

  addUser = async () => {
    try {
      await fetch('/api/users/add/?mail=' + this.state.user + '&password=' + this.state.password)

      console.log('bite')

    } catch (err) {
      sessionStorage.setItem("error", err)
    }
  }

  render() {
    return (
      <div className="w-25 rounded p-2 position-fixed"
           style={{opacity: 1, transform: "translate(150%, -50%)", backgroundColor: '#fff', zIndex: 15}}>
        <form>
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">Email</label>
            <Input type="text" className="form-control" placeholder="Enter email" onChangeHandler={this.onChangeUser}/>
            <small id="emailHelp" className="form-text text-muted">
              We'll never share your email with anyone else.
            </small>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Mot de passe</label>
            <Input type="password" className="form-control" placeholder="Mot de passe"
                   onChangeHandler={this.onChangePassword}/>
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword2">Confirmez le mot de passe</label>
            <Input type="password" className="form-control" placeholder="Mot de passe"/>
          </div>
          <BtnBase classArgs="btn btn-primary" text="S'inscrire" handlerOnClick={this.addUser}/>
        </form>
      </div>
    )
  }
}

export default ModalSubscribe
