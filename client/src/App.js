import React, {Component} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Header from './partials/Header'
import Body from "./partials/Body";
import Footer from "./partials/Footer";
import Modals from "./components/modals/Modals";
import MessagePrinter from "./partials/MessagePrinter";

class App extends Component {

  constructor(props) {
    super(props)
    this.onClickModalRenderer = this.onClickModalRenderer.bind(this)
    this.updateMessage = this.updateMessage.bind(this)
    this.disconnect = this.disconnect.bind(this)
    this.state = {
      dataType: "dino",
      idUser: 0,
      isConnected: false,
      modalShouldRender: false,
      modalToRender: 0,
      message: null
    }
  }

  updateMessage(message) {
    this.setState({message: message})
  }

  testCallSrv = async (userName, password) => {
    try {
      const res = await fetch('/api/users/?name=' + userName + '&password=' + password)

      const connect = await res.json()

      if (connect.connectStatus) {
        sessionStorage.setItem("error", "RAS DANS MA BIIIITE !")
        this.setState({isConnected: connect.connectStatus})
        this.setState({idUser: connect.idUser})
        sessionStorage.setItem("isConnected", JSON.stringify(this.state.isConnected))
      } else {
        sessionStorage.setItem("error", "Mauvais mdp")
      }
    } catch (err) {
      sessionStorage.setItem("error", err)
    }
  };

  componentWillUnmount() {
    sessionStorage.setItem("isConnected", JSON.stringify(this.state.isConnected))
  }

  componentDidMount() {
    this.setState({isConnected: sessionStorage.getItem("isConnected")})
    console.log('after mount: ' + this.state.isConnected)
    this.setState(() => {
      return {message: null}
    })
  }

  disconnect() {
    this.setState({isConnected: false})
    sessionStorage.setItem("isConnected", JSON.stringify(false))
  }

  onClickModalRenderer(data) {
    this.setState({
      modalShouldRender: !this.state.modalShouldRender,
      modalToRender: data
    })
  }

  render() {
    console.log(sessionStorage.getItem("error"))
    console.log(sessionStorage.getItem("isConnected"))
    let messagePrinter = this.state.message !== null ? (
      <MessagePrinter message={this.state.message} delay={3}/>
    ) : null
    return (
      <div className="App">
        <Header isConnected={this.state.isConnected} onClickHandler={this.testCallSrv}
                onClickModal={this.onClickModalRenderer} onClickDisconnect={this.disconnect}
                idUser={this.state.idUser}/>
        <Body isConnected={this.state.isConnected} currentData={this.state.dataType} idUser={this.state.idUser}
              updateMsg={this.updateMessage}/>
        <Footer isConnected={this.state.isConnected}/>
        <Modals shouldRender={this.state.modalShouldRender} handlerOnClick={this.onClickModalRenderer}
                modalToRender={this.state.modalToRender} addUser={this.addUser} connectUser={this.testCallSrv}/>
        {messagePrinter}
      </div>
    )
  }
}

export default App
