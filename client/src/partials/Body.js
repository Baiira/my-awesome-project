import React, {Component} from 'react';
import CardBase from "../components/card/CardBase";

class Body extends Component {
  constructor(props) {
    super(props)
    this.state = {
      array: []
    }
  }

  callApi = async () => {
    try {
      const res = await fetch('/api/dino')

      const connect = await res.json()

      connect.dinos.forEach((dino) => {
        this.setState(prevState => ({
          array: [...prevState.array, dino]
        }))
      })
      sessionStorage.setItem("array", JSON.stringify(this.state.array))
    } catch (err) {
      console.log("error dino " + err)
    }
  }

  componentDidMount() {
    let arrays = JSON.parse(sessionStorage.getItem("array"))
    if (arrays !== null && arrays.size > 0) {
      this.setState({array: arrays})
      console.log(this.state.array)
    } else {
      this.callApi()
    }
  }

  render() {
    const listDino = this.state.array.map((dino) =>
      <CardBase isLogged={this.props.isConnected} dino={dino} idUser={this.props.idUser} updateMsg={this.props.updateMsg.bind(this)}/>
    )
    return (
      <div>
        {this.props.currentData === "dino" && (
          listDino
        )}
      </div>
    )
  }
}

export default Body
