import React, {Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

class MessagePrinter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      seconds: this.props.delay
    }
  }

  tick() {
    this.setState(prevState => ({
      seconds: prevState.seconds - 1
    }));
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return this.state.seconds > 0 ? (
      <div className="alert alert-secondary" style={{height: 200, width: 400, opacity: 0.5, position: 'fixed', right: 30, bottom: 30}}>
        {this.props.message}
      </div>
    ) : null
  }
}

export default MessagePrinter