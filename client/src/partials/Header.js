import React, {Component} from 'react';
import LoginZone from "../components/header/LoginZone";
import Logout from "../components/header/Logout";

class Header extends Component {

  render() {
    const styleForm = {
      width: 300,
      lineHeight: 0,
      position: 'relative',
      backgroundColor: '#fff'
    };

    const styleLogZone = "w-100 d-inline-block"
    const isLogged = this.props.isConnected

    console.log(isLogged + " from header")

    if (isLogged) {
      console.log('oui ' + isLogged)
      return (

        <div className="w-100 py-2 h-auto d-inline-block" style={{backgroundColor: '#007191'}}>
          <div style={styleForm} className="float-right rounded mr-5">
            <Logout styleZone={styleLogZone} onClickHandler={this.props.onClickDisconnect.bind(this)} idUser={this.props.idUser}/>
          </div>
        </div>
      )
    } else {
      console.log('non ' + isLogged)
      return (
        <div className="w-100 py-2 h-auto d-inline-block" style={{backgroundColor: '#007191'}}>
          <div style={styleForm} className="float-right rounded mr-5">
            <LoginZone styleZone={styleLogZone} onClickHandler={this.props.onClickHandler.bind(this)}
                       onClickModal={this.props.onClickModal.bind(this)}/>
          </div>
        </div>
      )
    }

  }
}

export default Header
