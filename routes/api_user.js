const type = require('tedious').TYPES

let express = require('express')
let router = express.Router()

let Request = require('tedious').Request

let connection = require('../classes/database/dbConnection')
let format = require('../classes/database/dbFormatRequest')
let sql = require("../classes/database/sqlValue")
let userTable = require('../classes/database/dbArgs').user_table

/* GET users listing. */
router.get('/api/users/?', (req, res) => {
  let userName = req.query.name
  let password = req.query.password

  console.log("usr: " + userName)
  console.log("pwd: " + password)

  let request = new Request(format.Select([userTable.password, userTable.id])
      .From(userTable.tableName)
      .Where(userTable.firstName, sql.LIKE, 'name').request,
    (err, rowCount, row) => {
      let test = false
      let idUser = -1
      if (err)
        console.log(err)

      console.log(row[0][1].value)
      if (rowCount > 0) {
        if (password === row[0][0].value) {
          test = true
          idUser = row[0][1].value
        }
      }

      res.json({connectStatus: test, idUser: idUser})
    })

  request.addParameter('name', type.VarChar, userName);

  connection.execSql(request)
})

module.exports = router
