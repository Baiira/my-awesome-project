const type = require('tedious').TYPES

let express = require('express')
let router = express.Router()

let Request = require('tedious').Request

let connection = require('../classes/database/dbConnection')
let format = require('../classes/database/dbFormatRequest')
let sql = require("../classes/database/sqlValue")
let dinoTable = require('../classes/database/dbArgs').object_table
let dinoType = require('../classes/database/dbArgs').type_object_table

/* GET users listing. */
router.get('/api/dino/', (req, res) => {

  let request = new Request(format.Select([dinoTable.id + " as dinoKey", dinoType.name + " as dinoType", dinoTable.name + " as dinoName", dinoTable.price  + " as price", dinoTable.description + " as description"])
      .From([dinoTable.tableName, dinoType.tableName])
      .request,
    (err, rowCount, row) => {
      if (err)
        console.log(err)

      let dinos = []

      row.forEach((dino) => {
        dinos.push({
          key: dino[0].value,
          name: dino[1].value,
          type: dino[2].value,
          price: dino[3].value,
          description: dino[4].value
        })
      })

      res.json({dinos: dinos})
    })

  connection.execSql(request)
})

module.exports = router
