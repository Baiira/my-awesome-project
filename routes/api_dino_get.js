const type = require('tedious').TYPES

let express = require('express')
let router = express.Router()

let Request = require('tedious').Request

let connection = require('../classes/database/dbConnection')
let format = require('../classes/database/dbFormatRequest')
let sql = require("../classes/database/sqlValue")
let dinoTable = require('../classes/database/dbArgs').object_table
let userTable = require('../classes/database/dbArgs').user_table

/* GET users listing. */
router.get('/api/dino/get/?', (req, res) => {

  const id = req.query.id
  const idUser = req.query.idUser

  let request = new Request(format.Select([dinoTable.state + " as state", dinoTable.price + " as price"])
      .From(dinoTable.tableName)
      .Where(dinoTable.id, sql.EQUALS, 'id')
      .request,
    (err, rowCount, row) => {

      if (err)
        console.log(err)

      let state = row[0][0].value
      let price = row[0][1].value
      console.log(price + 'dino €')

      let requestUsr = new Request(format.Select(userTable.amount)
          .From(userTable.tableName)
          .Where(userTable.id, sql.EQUALS, 'id')
          .request,
        (err, rowCount, row) => {
          if (err) console.log(err)

          let userWallet = row[0][0].value
          console.log(userWallet + ': €')

          if (price < userWallet) {

            if (state === 0 || state === null) {
              let requestTwo = new Request(format.Update(dinoTable.tableName)
                .Set(dinoTable.state, 1)
                .Where(dinoTable.id, sql.EQUALS, 'id')
                .request, (err) => {
                if (err)
                  console.log(err)

                res.json({message: "Le dino a bien été acheté !"})

                let requestThree = new Request(format.Update(userTable.tableName)
                  .Set(userTable.amount, '@amount')
                  .Where(userTable.id, sql.EQUALS, 'id')
                  .request, (err) => {
                  if (err)
                    console.log(err)

                })
                requestThree.addParameter('id', type.Int, idUser)
                requestThree.addParameter('amount', type.Int, userWallet - price)

                connection.execSql(requestThree)
              })
              requestTwo.addParameter('id', type.Int, id)

              connection.execSql(requestTwo)
            } else {
              res.json({message: "Le dino a déjà été vendu !"})
            }
          } else {
            res.json({message: "Pas assez d'argent sur le compte"})
          }
        })

      requestUsr.addParameter('id', type.Int, idUser)

      connection.execSql(requestUsr)
    })

  request.addParameter('id', type.Int, id)

  connection.execSql(request)

})

module.exports = router
