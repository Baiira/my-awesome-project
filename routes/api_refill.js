const type = require('tedious').TYPES

let express = require('express')
let router = express.Router()

let Request = require('tedious').Request

let connection = require('../classes/database/dbConnection')
let format = require('../classes/database/dbFormatRequest')
let sql = require("../classes/database/sqlValue")
let userTable = require('../classes/database/dbArgs').user_table

/* GET users listing. */
router.get('/api/refill/', (req, res) => {
  let idUser = req.query.id

  console.log("usr: " + idUser)

  let request = new Request(format.Update(userTable.tableName).Set(userTable.amount, "amount + 5")
      .Where(userTable.id, sql.EQUALS, 'id').request,
    (err) => {
      if (err)
        console.log(err)

      res.json({name: "oui"})
    })

  request.addParameter('id', type.Int, idUser);

  connection.execSql(request)
})

module.exports = router
