const type = require('tedious').TYPES

let express = require('express')
let router = express.Router()

let Request = require('tedious').Request

let connection = require('../classes/database/dbConnection')
let format = require('../classes/database/dbFormatRequest')
let sql = require("../classes/database/sqlValue")
let userTable = require('../classes/database/dbArgs').user_table

/* GET users listing. */
router.get('/api/users/add/?', (req, res) => {
  let mail = req.query.mail
  let password = req.query.password

  console.log(format.Insert(userTable.tableName, [userTable.firstName, userTable.amount, userTable.password, userTable.city])
    .Values([mail, 0, 0, password, 1])
    .request)

  let request = new Request(format.Insert(userTable.tableName, [userTable.firstName, userTable.amount, userTable.password, userTable.city])
      .Values([mail, 0, 0, password, 1])
      .request,
    (err) => {
      if (err) console.log(err)

      res.status(200)
    })

  connection.execSql(request)

})

module.exports = router
