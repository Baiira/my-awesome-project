
First project made with ReactJs and NodeJs.

Was our first NodeJs REST API combine with a ReactJs front.

In order to run the project you need to have a MSSQL server running.

You can override database configuration in \classes\database\dbConfig.js.

We have made a small install script to load the database schema: npm run install

License under MIT.

