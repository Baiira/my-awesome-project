module.exports = {

  SELECT: 'SELECT ',
  INSERT: 'INSERT INTO',
  DELETE: 'DELETE ',
  DROP: 'DROP ',
  UPDATE: 'UPDATE ',
  FROM: 'FROM ',
  SET: 'SET ',
  VALUES: 'VALUES ',

// A few punctuation signs
  SPACE: ' ',
  SEPARATOR: ', ',
  ALL: '* ',
  QUOTE: '\'',
  FINAL: ';',
  OPEN_PAR: '(',
  CLOSE_PAR: ')',

// Operator for WHERE clause
  WHERE: 'WHERE ',
  EQUALS: '=',
  MORE_THAN: '>',
  LESS_THAN: '<',
  AND: 'AND',
  OR: 'OR',
  NOT_EQUALS: '!=',
  MORE_OR_EQUAL: '>=',
  LESS_OR_EQUAL: '<=',
  IN: 'IN',
  BETWEEN: 'BETWEEN',
  LIKE: 'LIKE',
  IS_NULL: 'IS NULL',
  IS_NOT_NULL: 'IS NOT NULL',

// ORDER format
  ORDER: 'ORDER BY ',
  DESC: 'DESC ',
  ASC: 'ASC '
}