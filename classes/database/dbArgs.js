/**
 * Extension à rajouter devant chaques tables.
 */
const ext = 'sa1.',
  user = ext + 'USERS',
  city = ext + 'CITY',
  promise = ext + 'PROMISE',
  typeObj = ext + 'TYPEOBJECT',
  object = ext + 'OBJECT'

module.exports = {

  /**
   * Table utilisateur
   */
  user_table: {
    tableName: user,
    id: user + '.ID_USER',
    firstName: user + '.FIRSTNAME',
    lastName: user + '.LASTNAME',
    password: user + '.PASSWORD',
    amount: user + '.AMOUNT',
    mail: user + '.MAIL',
    sex: user + '.SEXE',
    accessLevel: user + '.ACCESSLEVEL',
    birthDate: user + '.BIRTHDATE',
    city: user + '.ID_CITY'
  },

  /**
   * Table promesses
   */
  promise_table: {
    tableName: promise,
    id: promise + '.ID_PROMISE',
    userId: promise + '.ID_USER',
    objectId: promise + '.ID_OBJECT',
    status: promise + '.STATUS',
    datePromise: promise + '.DATEPROMISE'
  },

  /**
   * Table type de l'objet
   */
  type_object_table: {
    tableName: typeObj,
    id: typeObj + '.ID_TYPE',
    name: typeObj + '.NAME',
    image: typeObj + '.IMAGE'
  },

  /**
   * Table objet
   */
  object_table: {
    tableName: object,
    id: object + '.ID_OBJECT',
    idType: object + '.ID_TYPE',
    idUser: object + '.ID_USER',
    name: object + '.NAME',
    state: object + '.STATE',
    dateSale: object + '.DATESALE',
    idVendor: object + '.VENDORID',
    description: object + '.DESCRIPTION',
    price: object + '.PRICE',
    image: object + '.IMAGE'
  },

  /**
   * Table ville
   */
  city_table: {
    tableName: city,
    id: city + '.ID_CITY',
    name: city + '.NAME',
    postal: city + '.POSTAL'
  },
}