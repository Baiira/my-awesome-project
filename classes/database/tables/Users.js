let tedious = require('tedious')
let Request = tedious.Request
let connection = require('../dbController')
let userTable = require('../dbArgs').user_table
let sql = require('../sqlValue')
let format = require('../dbFormatRequest')

class Users {
  constructor() {
    this.id = 0
    this.fName = ""
    this.lName = ""
    this.sex = true
    this.birthDate = new Date()
    this.adress = ""
    this.password = ""
    this.mail = ""
    this.amount = 0
    this.accessLevel = 0
    this.city = 0
  }

  static GetUser(id) {
    let results = []
    let formatRequest = format.Select().From(userTable.tableName).Where(userTable.id, sql.EQUALS, this.id)
    let request = new Request(formatRequest, (err, col) => {
      if (err) {
        throw err
      }
      results.push(col)
    })

    connection.execSql
    return new Users().CreateUser(results);
  }

  CreateUser(table) {

  }
}