let tedious = require('tedious')
let Connection = tedious.Connection
let config = require('./dbConfig')

let connection = new Connection(config)
connection.on('infoMessage', (info) => {
  if (info) {
    console.log('Info: ' + info.message)
  }
  console.log('No info')
})
connection.on('debug', (msgTxt) => {
  if (msgTxt) {
    console.log('debug: ' + msgTxt)
  }
  console.log('no debug')
})
connection.on('errorMessage', (err) => {
  if (err) {
    console.log('Error msg')
    throw err
  }
  console.log('No error msg')
})

module.exports = connection