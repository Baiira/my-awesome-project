const sql = require('./sqlValue')

class FormatClass {
  // First hand arg for request
  constructor() {
    this.request = ""
  }

  /**
   * @desc Cette fonction permet de formater une requête de type SELECT en lui passant comme arguments.
   * @author F.DAL-POS
   * @param colToSelect {array | string} Colonne(s) à séléctionner.
   * @return {FormatClass}
   */
  Select(colToSelect = null) {
    /**
     * Request like 'SELECT '
     */
    this.request = sql.SELECT
    if (colToSelect !== null) {
      if (colToSelect.length > 0 && Array.isArray(colToSelect)) {
        colToSelect.forEach((col, current, array) => {
          this.request += col
          if (current < array.length - 1) {
            this.request += sql.SEPARATOR
          }
        })
      } else {
        this.request += colToSelect
      }
    } else {
      this.request += sql.ALL
    }
    /**
     * Request like 'SELECT * '
     */
    return this
  }

  From(tableFrom) {
    this.request += sql.SPACE + sql.FROM
    /**
     * Request like 'SELECT * FROM '
     */
    if (Array.isArray(tableFrom)) {
      tableFrom.forEach((col, current, array) => {
        this.request += col
        if (current < array.length - 1) {
          this.request += sql.SEPARATOR
        }
      })
    } else {
      this.request += tableFrom + sql.SPACE
    }
    /**
     * Request like 'SELECT * FROM foo '
     */
    return this
  }

  /**
   * @date 13/04/2018
   * @author F.DAL-POS
   * @desc Permet un formattage de la clause WHERE de la requête.
   * @param leftOp {string} Opérateur de gauche pour la clause WHERE
   * @param operator {string} Opérateur pour la clause WHERE
   * @param rightOp {Object} Opérateur de droite pour la clause WHERE
   * @return {FormatClass}
   *
   */
  Where(leftOp, operator, rightOp) {
    if (leftOp === null || operator === null || rightOp === null) {
      throw new Error()
    }
    let whereClause = sql.WHERE + sql.SPACE

    whereClause += leftOp + sql.SPACE

    switch (operator) {
      case sql.EQUALS:
      case sql.MORE_OR_EQUAL:
      case sql.MORE_THAN:
      case sql.LESS_OR_EQUAL:
      case sql.LESS_THAN:
      case sql.LIKE:
      case sql.IS_NULL:
      case sql.NOT_EQUALS:
      case sql.IS_NOT_NULL:
      case sql.BETWEEN:
      case sql.IN:
        whereClause += operator + sql.SPACE
        break
      default:
        throw new Error()
    }
    whereClause += '@' + rightOp
    this.request += whereClause
    return this
  }

  /**
   *
   * @param args {array | string} Colonne(s) à order pour la requête.
   * @param orderBy {array | string | null} Ordre dans lequel il faut order le retour. Default ASC
   * @return {FormatClass}
   */
  Order(args, orderBy = null) {
    let toReturn = sql.SPACE + sql.ORDER + sql.SPACE
    if (args !== null) {
      if (Array.isArray(args)) {
        for (let i = 0; i < args.length; i++) {
          if (Array.isArray(orderBy)) {
            if (orderBy[i] !== null) {
              toReturn += this.OrderTest(args[i], orderBy[i])
            } else {
              toReturn += this.OrderTest(args[i], sql.ASC)
            }
          } else {
            toReturn += this.OrderTest(args[i], orderBy)
          }
          if (i < args.length - 1) {
            toReturn += sql.SEPARATOR
          }
          toReturn += sql.SPACE
        }
      } else {
        if (orderBy !== null) {
          toReturn += this.OrderTest(args, orderBy)
        } else {
          toReturn += this.OrderTest(args, sql.ASC)
        }
      }
    }
    this.request += toReturn
    return this
  }

  /**
   * PRIVATE METHOD DO NOT USE !!!
   * @param col
   * @param orderBy
   * @return {string}
   */
  OrderTest(col, orderBy) {
    if (orderBy !== null && orderBy !== "" && orderBy !== undefined) {
      return col + sql.SPACE + orderBy
    } else {
      return col + sql.SPACE + sql.ASC
    }
  }

  /**
   * Cette méthode DOIT TOUJOURS être suivie par la methode Set()
   * @param table {string} Table à update.
   * @return {FormatClass}
   */
  Update(table) {
    if (table !== null && !Array.isArray(table)) {
      this.request = sql.UPDATE + table + sql.SPACE
    } else {
      throw new Error()
    }
    return this
  }

  /**
   * Cette methode ne défini pas de condition pour l'UPDATE donc il vaut mieux la faire suivre par la méthode Where()
   * @param colToUpdate Colonne de la table à update.
   * @param value Valeur à set pour la colonne
   * @return {FormatClass}
   */
  Set(colToUpdate, value) {
    if (colToUpdate !== null && value !== null) {
      this.request += sql.SET + sql.SPACE
      if (Array.isArray(colToUpdate)) {
        if (Array.isArray(value)) {
          for (let i = 0; i < colToUpdate.length; i++) {
            this.request += colToUpdate[i] + sql.EQUALS + sql.SPACE + value[i]
          }
        } else {
          throw new Error()
        }
      } else {
        this.request += colToUpdate + sql.SPACE + sql.EQUALS + sql.SPACE + value + sql.SPACE
      }
    } else {
      throw new Error()
    }
    return this
  }

  /**
   * Cette méthode ne défnini pas de condition, à utiliser suivi de la methode Where()
   * @param tableFrom {string} Table à supprimer
   * @return {FormatClass}
   */
  Delete(tableFrom) {
    if (tableFrom !== null) {
      this.request = sql.DELETE + sql.FROM + sql.SPACE
      this.request += tableFrom + sql.SPACE
      return this
    }
    throw new Error()
  }

  /**
   *
   * @param table {string} Table dans laquelle la modification doit être effectué.
   * @param colToInsert {string | array | null} Les colonnes de la table à ajouter. Par défaut toutes les colonnes doivent l'être.
   * @return {FormatClass}
   *
   */
  Insert(table, colToInsert = null) {
    if (table !== null) {
      this.request = sql.INSERT + sql.SPACE
      if (colToInsert !== null) {
        this.request += sql.OPEN_PAR
        if (Array.isArray(colToInsert)) {
          colToInsert.forEach((col) => {
            this.request += col + sql.SEPARATOR
          })
        } else {
          this.request += colToInsert + sql.SEPARATOR
        }
        this.request += sql.CLOSE_PAR + sql.SPACE
      }
      console.log("You should not let the second arg null and define the columns to insert cause it can lead to errors if there is more Values than columns")
      return this
    }
    throw new Error()
  }

  /**
   *
   * @param values {string | int | array} Les valeurs à ajouter pour la ligne, de base il faut les mettre dans l'ordre de la table.
   * @return {FormatClass}
   */
  Values(values) {
    if (values !== null) {
      this.request += sql.VALUES + sql.OPEN_PAR
      if (Array.isArray(values)) {
        values.forEach((col, current, array) => {
          if (typeof values === 'string' || values instanceof String) {
            this.request += sql.QUOTE + col + sql.QUOTE
            if (current < array.length) {
              this.request += sql.SEPARATOR
            }
          }
        })
      } else {
        if (typeof values === 'string' || values instanceof String) {
          this.request += sql.QUOTE + values + sql.QUOTE
        }
      }
      this.request += sql.CLOSE_PAR + sql.SPACE
      return this
    }
    throw new Error()
  }
}

module.exports = new FormatClass()