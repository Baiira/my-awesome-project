module.exports.default = {
  script:
  "if exists (select 1\n" +
  "   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')\n" +
  "   where r.fkeyid = object_id('OBJECT') and o.text = 'FK_OBJECT_IS_TYPEOBJE')\n" +
  "alter table OBJECT\n" +
  "   drop constraint FK_OBJECT_IS_TYPEOBJE\n" +
  "if exists (select 1\n" +
  "   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')\n" +
  "   where r.fkeyid = object_id('PROMISE') and o.text = 'FK_PROMISE_PROMISE_USER')\n" +
  "alter table PROMISE\n" +
  "   drop constraint FK_PROMISE_PROMISE_USER\n" +
  "if exists (select 1\n" +
  "   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')\n" +
  "   where r.fkeyid = object_id('PROMISE') and o.text = 'FK_PROMISE_PROMISE2_OBJECT')\n" +
  "alter table PROMISE\n" +
  "   drop constraint FK_PROMISE_PROMISE2_OBJECT\n" +
  "if exists (select 1\n" +
  "   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')\n" +
  "   where r.fkeyid = object_id('\"USERS\"') and o.text = 'FK_USER_LIVE_CITY')\n" +
  "alter table \"USERS\"\n" +
  "   drop constraint FK_USER_LIVE_CITY\n" +
  "if exists (select 1\n" +
  "            from  sysobjects\n" +
  "           where  id = object_id('CITY')\n" +
  "            and   type = 'U')\n" +
  "   drop table CITY\n" +
  "if exists (select 1\n" +
  "            from  sysindexes\n" +
  "           where  id    = object_id('OBJECT')\n" +
  "            and   text  = 'IS_FK'\n" +
  "            and   indid > 0\n" +
  "            and   indid < 255)\n" +
  "   drop index OBJECT.IS_FK\n" +
  "if exists (select 1\n" +
  "            from  sysobjects\n" +
  "           where  id = object_id('OBJECT')\n" +
  "            and   type = 'U')\n" +
  "   drop table OBJECT\n" +
  "if exists (select 1\n" +
  "            from  sysindexes\n" +
  "           where  id    = object_id('PROMISE')\n" +
  "            and   text  = 'PROMISE2_FK'\n" +
  "            and   indid > 0\n" +
  "            and   indid < 255)\n" +
  "   drop index PROMISE.PROMISE2_FK\n" +
  "if exists (select 1\n" +
  "            from  sysindexes\n" +
  "           where  id    = object_id('PROMISE')\n" +
  "            and   text  = 'PROMISE_FK'\n" +
  "            and   indid > 0\n" +
  "            and   indid < 255)\n" +
  "   drop index PROMISE.PROMISE_FK\n" +
  "if exists (select 1\n" +
  "            from  sysobjects\n" +
  "           where  id = object_id('PROMISE')\n" +
  "            and   type = 'U')\n" +
  "   drop table PROMISE\n" +
  "if exists (select 1\n" +
  "            from  sysobjects\n" +
  "           where  id = object_id('TYPEOBJECT')\n" +
  "            and   type = 'U')\n" +
  "   drop table TYPEOBJECT\n" +
  "if exists (select 1\n" +
  "            from  sysindexes\n" +
  "           where  id    = object_id('\"USERS\"')\n" +
  "            and   text  = 'LIVE_FK'\n" +
  "            and   indid > 0\n" +
  "            and   indid < 255)\n" +
  "   drop index \"USERS\".LIVE_FK\n" +
  "if exists (select 1\n" +
  "            from  sysobjects\n" +
  "           where  id = object_id('\"USERS\"')\n" +
  "            and   type = 'U')\n" +
  "   drop table \"USERS\"\n" +
  "create table CITY (\n" +
  "   ID_CITY              int                  not null,\n" +
  "   NAME                 text                 null,\n" +
  "   POSTAL               int                  null,\n" +
  "   constraint PK_CITY primary key (ID_CITY)\n" +
  ")\n" +
  "create table OBJECT (\n" +
  "   ID_OBJECT            int                  not null,\n" +
  "   ID_TYPE              int                  not null,\n" +
  "   ID_USER              int                  null,\n" +
  "   NAME                 text                 null,\n" +
  "   STATE                smallint             null,\n" +
  "   DATESALE             datetime             null,\n" +
  "   VENDORID             int                  null,\n" +
  "   DESCRIPTION          text                 null,\n" +
  "   PRICE                int                  null,\n" +
  "   IMAGE                text                 null,\n" +
  "   constraint PK_OBJECT primary key (ID_OBJECT)\n" +
  ")\n" +
  "create nonclustered index IS_FK on OBJECT (ID_TYPE ASC)\n" +
  "create table PROMISE (\n" +
  "   ID_PROMISE           int                  not null,\n" +
  "   ID_USER              int                  not null,\n" +
  "   ID_OBJECT            int                  not null,\n" +
  "   STATUS               bit                  null,\n" +
  "   DATEPROMISE          datetime             null,\n" +
  "   constraint PK_PROMISE primary key (ID_PROMISE)\n" +
  ")\n" +
  "create nonclustered index PROMISE_FK on PROMISE (ID_USER ASC)\n" +
  "create nonclustered index PROMISE2_FK on PROMISE (ID_OBJECT ASC)\n" +
  "create table TYPEOBJECT (\n" +
  "   ID_TYPE              int                  not null,\n" +
  "   NAME                 text                 null,\n" +
  "   IMAGE                text                 null,\n" +
  "   constraint PK_TYPEOBJECT primary key (ID_TYPE)\n" +
  ")\n" +
  "create table \"USERS\" (\n" +
  "   ID_USER              int                  not null,\n" +
  "   LASTNAME             text                 null,\n" +
  "   FIRSTNAME            text                 null,\n" +
  "   PASSWORD             text                 null,\n" +
  "   AMOUNT               int                  null,\n" +
  "   MAIL                 text                 null,\n" +
  "   SEXE                 bit                  null,\n" +
  "   ACCESSLEVEL          smallint             null,\n" +
  "   BIRTHDATE            datetime             null,\n" +
  "   ID_CITY              int                  not null,\n" +
  "   constraint PK_USER primary key (ID_USER)\n" +
  ")\n" +
  "create nonclustered index LIVE_FK on \"USERS\" (ID_CITY ASC)\n" +
  "alter table OBJECT\n" +
  "   add constraint FK_OBJECT_IS_TYPEOBJE foreign key (ID_TYPE)\n" +
  "      references TYPEOBJECT (ID_TYPE)\n" +
  "alter table PROMISE\n" +
  "   add constraint FK_PROMISE_PROMISE_USER foreign key (ID_USER)\n" +
  "      references \"USERS\" (ID_USER)\n" +
  "alter table PROMISE\n" +
  "   add constraint FK_PROMISE_PROMISE2_OBJECT foreign key (ID_OBJECT)\n" +
  "      references OBJECT (ID_OBJECT)\n" +
  "alter table \"USERS\"\n" +
  "   add constraint FK_USER_LIVE_CITY foreign key (ID_CITY)\n" +
  "      references CITY (ID_CITY)\n"
}
