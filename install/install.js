let install = require('./dbScript').default.script
let sender = require('../classes/database/dbConnection')
let Request = require('tedious').Request

let request = new Request(install, (err) => {
  if (err) {
    console.log('Error installing database!')
    throw err
  }
})

sender.on('connect', () => {
  sender.execSql(request)
})